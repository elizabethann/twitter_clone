require 'test_helper'

class RelationsControllerTest < ActionController::TestCase
 
  test "create should require logged-in user" do
    assert_no_difference 'Relation.count' do
      post :create
    end
    assert_redirected_to login_url
  end

  test "destroy should require logged-in user" do
    assert_no_difference 'Relation.count' do
      delete :destroy, id: relations(:one)
    end
    assert_redirected_to login_url
  end
end
