require 'test_helper'

class RelationTest < ActiveSupport::TestCase
  def setup
    @relation = Relation.new(follr_id: 1, folld_id: 2)
  end

  test "should be valid" do
    assert @relation.valid?
  end

  test "should require a follower_id" do
    @relation.follr_id = nil
    assert_not @relation.valid?
  end

  test "should require a followed_id" do
    @relation.folld_id = nil
    assert_not @relation.valid?
  end
end
