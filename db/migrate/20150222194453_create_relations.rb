class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.string :follr_id
      t.string :integer
      t.integer :folld_id

      t.timestamps null: false
    end
    add_index :relations, :follr_id
    add_index :relations, :folld_id
    add_index :relations, [:follr_id, :folld_id], unique: true
  end
end
