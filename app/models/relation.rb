class Relation < ActiveRecord::Base
  belongs_to :follr, class_name: "User"
  belongs_to :folld, class_name: "User"
  validates :follr_id, presence: true
  validates :folld_id, presence: true
end
